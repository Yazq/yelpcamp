var express        = require("express"),
    request        = require("request"),
    bodyParser     = require("body-parser"),
    mongoose       = require("mongoose"),
    flash          = require("connect-flash"),
    seedDB         = require("./seeds"),
    methodOverride = require("method-override"),
    passport       = require("passport"),
    LocalStrategy  = require("passport-local"),
    Campground     = require("./models/campground"),
    Comment        = require("./models/comment"),
    User           = require("./models/user"),
    app            = express();

//requiring routes
var campgroundsRoutes = require("./routes/campgrounds"),
    commentsRoutes    = require("./routes/comments"),
    indexRoutes       = require("./routes/index");


// DB Connection
mongoose.set('useFindAndModify', false);
mongoose.set("useUnifiedTopology", true);
mongoose.connect("mongodb+srv://admin:Zxnmcvb22.@cluster0-7xz1p.mongodb.net/test?retryWrites=true&w=majority", {
  useNewUrlParser: true
});

//PASSPORT CONFIGURATION

app.use(require("express-session")({
  secret: "Once again secret",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// use static authenticate method of model in LocalStrategy
passport.use(new LocalStrategy(User.authenticate()));


//NPM packages
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(methodOverride("_method"));
app.use(flash());

//Seed the Database
// seedDB();

// Serving Public Directory
app.use(express.static(__dirname + "/public"));

// middleware to pass current user info
app.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});

app.use("/", indexRoutes);
app.use("/campgrounds", campgroundsRoutes);
app.use("/campgrounds/:id/comments", commentsRoutes);


app.listen(process.env.PORT, process.env.IP, function() {
  console.log("server's running");
});
