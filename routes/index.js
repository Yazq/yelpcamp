var express    = require("express"),
    router     = express.Router(),
    passport   = require("passport"),
    Campground = require("../models/campground"),
    Comment    = require("../models/comment"),
    User       = require("../models/user");


//Root route
router.get("/", function(req, res) {
  res.render("home");
  // res.redirect("/campgrounds");
});

//show register form
router.get("/register", function(req, res) {
  res.render("register");
});

//handle sign up logic
router.post("/register", function(req, res) {
  var newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, function(err, user) {
    if(err) {
      req.flash("error", err.message);
      return res.redirect("register");
    }
    passport.authenticate("local")(req, res, function() {
      req.flash("success", "Welcome to YelpCamp " + user.username);
      res.redirect("/campgrounds");
    });
  });
});

//login form
router.get("/login", function(req, res) {
  res.render("login");
});

//handle login form
router.post("/login", function(req, res, next) { passport.authenticate("local",
  {
    successRedirect: "/campgrounds",
    failureRedirect: "/login",
    failureFlash: true,
    successFlash: "Welcome to YelpCamp " + req.body.username + "!"
  })(req, res);
});

//logout
router.get( "/logout", function(req, res) {
  req.logout();
  req.flash("success", "You logged out!");
  res.redirect("/campgrounds");
});


function isLoggedIn(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }
  res.redirect("/login");
};

module.exports = router;
