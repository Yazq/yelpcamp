var express    = require("express"),
    router     = express.Router({mergeParams: true}),
    Campground = require("../models/campground"),
    Comment    = require("../models/comment"),
    User       = require("../models/user"),
    middleware = require("../middleware");

//Comments New
router.get("/new", middleware.isLoggedIn, function(req, res) {
  Campground.findById(req.params.id, function(err, campground) {
    if(err) {
      console.log(err);
    } else {
      res.render("comments/new", {campground: campground});
    }
  });
});

//Comments Create
router.post("/", middleware.isLoggedIn, function(req, res) {
  Campground.findById(req.params.id, function(err, campground) {
    if(err) {
      console.log(err);
      res.redirect("/campgrounds");
    } else {
      var comment = req.body.comment;
      Comment.create(comment, function(err, comment) {
        if(err) {
          req.flash("error", "Something went wrong!");
          console.log(err);
        } else {
          comment.author.id = req.user._id;
          comment.author.username = req.user.username;
          comment.save();
          campground.comments.push(comment);
          campground.save();
          req.flash("success", "Comment created!");
          res.redirect("/campgrounds/" + campground._id);
        }
      });
    }
  });
});

//Comments edit route
router.get("/:comment_id/edit", middleware.checkCommentsOwnership, function(req, res) {
  Campground.findById(req.params.id, function(err, foundCampground) {
    if(err || !foundCampground) {
      req.flash("error", "Campground not found!");
      res.redirect("back");
    }
    Comment.findById(req.params.comment_id, function(err, foundComment) {
      res.render("comments/edit", {campground_id: req.params.id, comment: foundComment});
    });
  });
});

//UPDATE
router.put("/:comment_id", middleware.checkCommentsOwnership, function(req, res) {
  Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment) {
    req.flash("success", "Comment edited!")
    res.redirect("/campgrounds/" + req.params.id);
  });
});

//DESTROY
router.delete("/:comment_id", middleware.checkCommentsOwnership, function(req, res) {
  Comment.findByIdAndRemove(req.params.comment_id, function(err) {
    req.flash("success", "Comments deleted!")
    res.redirect("/campgrounds/" + req.params.id);
  });
});

module.exports = router;
