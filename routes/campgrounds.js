var express    = require("express"),
    router     = express.Router(),
    Campground = require("../models/campground"),
    Comment    = require("../models/comment"),
    middleware = require("../middleware");

//INDEX route - GET - /something - displays stuff
router.get("/", function(req, res) {
  Campground.find({}, function(err, camp) {
    if(err || !camp) {
      req.flash("error", "Campground not found!");
    } else {
      res.render("campgrounds/index", {campground: camp});
    }
  });
});

//CREATE route - POST /something - create new, same url as INDEX
router.post("/", middleware.isLoggedIn, function(req, res) {
  //grabbing data from form and adding to database
  var name = req.body.name;
  var image = req.body.image;
  var description = req.body.description;
  var price = req.body.price;
  var author = {
    id: req.user._id,
    username: req.user.username
  };
  var newCampground = {name: name, image: image, description: description, price: price, author: author};
  //Create a new campground and save to db
  Campground.create(newCampground, function(err, newCamp) {
    if(err) {
      res.flash("error", err.message);
      res.redirect("/campgrounds");
    } else {
      //redirect to campgrounds page
      res.redirect("/campgrounds");
    }
  })
});

//NEW route - GET - show form to create something
router.get("/new", middleware.isLoggedIn, function(req, res) {
  res.render("campgrounds/new");
});
//SHOW route - GET - more info about stuff
router.get("/:id", function(req, res) {
  Campground.findById(req.params.id).populate("comments").exec(function(err, campground) {
    if(err || !campground) {
      req.flash("error", "Campground not found!");
      res.redirect("/campgrounds");
    } else {
      res.render("campgrounds/show", {campground: campground});
    }
  });
});

//EDIT
router.get("/:id/edit", middleware.checkOwnership, function(req, res) {
  Campground.findById(req.params.id, function(err, foundCampground) {
        res.render("campgrounds/edit", {campground: foundCampground});
  });
});

//UPDATE
router.put("/:id", middleware.checkOwnership, function(req, res) {
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground) {
    res.redirect("/campgrounds/" + req.params.id);
  });
});

//DESTROY
router.delete("/:id", middleware.checkOwnership, function(req, res) {
  Campground.findByIdAndRemove(req.params.id, function(err) {
      res.redirect("/campgrounds");
  });
});

module.exports = router;
