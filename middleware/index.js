var Campground = require("../models/campground"),
    Comment    = require("../models/comment");
    User       = require("../models/user");

var middlewareObj = {};

middlewareObj.checkOwnership = function(req, res, next) {
  if(req.isAuthenticated()) {
    Campground.findById(req.params.id, function(err, foundCampground) {
      if(err || !foundCampground) {
        req.flash("error", "Campground not found!");
        res.redirect("back");
      } else {
        if(foundCampground.author.id.equals(req.user._id)) {
          next();
        } else {
          req.flash("error", "You dont have permission to do that!");
          res.redirect("back");
        }
      }
    });
  } else {
    req.flash("error", "You not suppose to be here!");
    res.redirect("back");
  }
};

middlewareObj.checkCommentsOwnership = function(req, res, next) {
  if(req.isAuthenticated()) {
    Comment.findById(req.params.comment_id, function(err, foundComment) {
      if(err || !foundComment) {
        console.log(err);
        req.flash("error", "Comment not found!");
        res.redirect("back");
      } else {
        if(foundComment.author.id.equals(req.user._id)) {
          next();
        } else {
          req.flash("error", "You dont have permission to do that!");
          res.redirect("back");
        }
      }
    });
  } else {
    req.flash("error", "You should not be here!");
    res.redirect("back");
  }
};

middlewareObj.isLoggedIn = function(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }
  req.flash("error", "You need to be logged in in order do that!");
  res.redirect("/login");
};

module.exports = middlewareObj;
